<?php 
        $id = $_GET['id'];
        $sql = "SELECT * FROM t_siswa WHERE NIS = $id";
        $query = $mysql->db->query($sql);
        $result = mysqli_fetch_object($query);
?>
<html>
	<head>
		<title>Ubah Siswa</title>
  <style type="text/css">
      td {
        font-family : ubuntu;
      }
  </style>
	</head>
	<body>
    <a href="index.php">Kembali</a><br>
		<h1 align="center" style="font-family : ubuntu;">Ubah Siswa</h1>
		<form enctype="multipart/form-data" action="crud/edit_tsiswa.php?id=<?php echo "$id"; ?>" method="post">
    <table align="center">
    <tbody>
    <tr>
     <td>&nbsp;</td>
     <td>
  <?php
    echo "<img src='avatar/";
    if($result->avatar==""){
      echo "noimage.png";
    }else{
      echo "$result->avatar";
    }
    echo "' width=100px heigth=100px /><br><br>";
  ?></td>
      </tr>
       <tr>
        <td>Nama Siswa
        </td>	
        <td><input name="txtnama" size="40" type="text" placeholder="Contoh Ali Qornan.." value="<?php echo "$result->nama_siswa"; ?>" ></td>
      </tr>
       <tr>
        <td>Tanggal lahir
        </td>	
        <td><input id="txttanggal" name="txttanggal" type="date" class="hasDatepicker" value="<?php echo "$result->tanggal_lahir"; ?>"></td>
        </tr>
         <tr>
        <td>Jenis Kelamin</td>
        <td>
        	<input name="rdbjk" value="L" type="radio" <?php if($result->jenis_kelamin=="L"){ echo"checked"; } ?> >Laki-laki
        	<input name="rdbjk" value="P" type="radio" <?php if($result->jenis_kelamin=="P"){ echo"checked"; } ?> >Perempuan
        </td>
      </tr>
       <tr>
        <td>Email
        </td>	
        <td><input name="txtemail" size="40" type="text" placeholder="Contoh aliqornan@ymail.com.." value="<?php echo "$result->email"; ?>"></td>
      </tr>
      <tr>
        <td>No. Telp
        </td>	
        <td><input name="txttelp" size="40" type="text" placeholder="Contoh 12345678.." value="<?php echo "$result->nomor_telepon"; ?>"></td>
      </tr>
      <tr>
        <td>Avatar
        </td> 
        <td><input name="file" id="avatar" type="file"></td>
      </tr>
      <tr>
        <td>Kelas
        </td>
        <td>
          <select name="cmbkelas">
          	<?php
          		$sql = "SELECT * FROM t_kelas ORDER BY nama_kelas asc";
				$query = $mysql->db->query($sql);
				while ($result2 = mysqli_fetch_object($query)){
					echo  "<option value='$result2->id_kelas' ";
          if($result->id_kelas==$result2->id_kelas){ echo "selected";}
          echo ">$result2->nama_kelas</option>";
				}
          	?>
          </select> 
        </td>
      </tr>
      <tr>
        <td>Kelompok
        </td>
        <td>
          <select name="cmbkelompok">
           <?php
          		$sql = "SELECT * FROM t_kelompok ORDER BY nama_kelompok asc";
				$query = $mysql->db->query($sql);
				while ($result3 = mysqli_fetch_object($query)){
					echo  "<option value='$result3->id_kelompok' ";
          if($result->id_kelompok==$result3->id_kelompok){ echo "selected";}
          echo ">$result3->nama_kelompok</option>";
				}
          	?>
          </select>
        </td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td><Button type="submit">Submit</Button></td>
      </tr>
    </tbody>
  </table>
</form>
	</body>
</html>