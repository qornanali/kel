<html>
	<head>
		<title>Tambah Siswa</title>
  <style type="text/css">
      td {
        font-family : ubuntu;
      }
  </style>
	</head>
	<body>
    <a href="index.php">Kembali</a><br>
		<h1 align="center" style="font-family : ubuntu;">Tambah Siswa</h1>
		<form enctype="multipart/form-data" action="crud/insert_tsiswa.php" method="post">
  <table align="center">
    <tbody>
      <tr>
        <td>N I S</td>
        <td><input name="txtnis" size="40" type="text" placeholder="Contoh 1213114855.." required></td>
      </tr>
       <tr>
        <td>Nama Siswa
        </td>	
        <td><input name="txtnama" size="40" type="text" placeholder="Contoh Ali Qornan.." required></td>
      </tr>
       <tr>
        <td>Tanggal lahir
        </td>	
        <td><input id="txttanggal" name="txttanggal" type="date" class="hasDatepicker" required></td>
        </tr>
         <tr>
        <td>Jenis Kelamin</td>
        <td>
        	<input name="rdbjk" value="L" type="radio" required>Laki-laki
        	<input name="rdbjk" value="P" type="radio" required>Perempuan
        </td>
      </tr>
       <tr>
        <td>Email
        </td>	
        <td><input name="txtemail" size="40" type="text" placeholder="Contoh aliqornan@ymail.com.." ></td>
      </tr>
      <tr>
        <td>No. Telp
        </td>	
        <td><input name="txttelp" size="40" type="text" placeholder="Contoh 12345678.." ></td>
      </tr>
      <tr>
        <td>Avatar
        </td> 
        <td><input name="file" id="avatar" type="file" ></td>
      </tr>
      <tr>
        <td>Kelas
        </td>
        <td>
          <select name="cmbkelas">
          	<?php
          		$sql = "SELECT * FROM t_kelas ORDER BY nama_kelas asc";
				$query = $mysql->db->query($sql);
				while ($result = mysqli_fetch_object($query)){
					echo  "<option value='$result->id_kelas'>$result->nama_kelas</option>";
				}
          	?>
          </select> 
        </td>
      </tr>
      <tr>
        <td>Kelompok
        </td>
        <td>
          <select name="cmbkelompok">
           <?php
          		$sql = "SELECT * FROM t_kelompok ORDER BY nama_kelompok asc";
				$query = $mysql->db->query($sql);
				while ($result = mysqli_fetch_object($query)){
					echo  "<option value='$result->id_kelompok'>$result->nama_kelompok</option>";
				}
          	?>
          </select>
        </td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td><Button type="submit">Submit</Button></td>
      </tr>
    </tbody>
  </table>
</form>
	</body>
</html>